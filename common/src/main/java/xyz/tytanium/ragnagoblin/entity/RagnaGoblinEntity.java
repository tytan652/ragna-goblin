package xyz.tytanium.ragnagoblin.entity;

import com.mrcrayfish.goblintraders.entity.AbstractGoblinEntity;
import com.mrcrayfish.goblintraders.trades.EntityTrades;
import com.mrcrayfish.goblintraders.trades.IRaritySettings;
import com.mrcrayfish.goblintraders.trades.TradeManager;
import com.mrcrayfish.goblintraders.trades.TradeRarity;
import net.minecraft.ChatFormatting;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.trading.MerchantOffers;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.level.Level;
import xyz.tytanium.ragnagoblin.Config;
import xyz.tytanium.ragnagoblin.Constants;
import xyz.tytanium.ragnagoblin.core.ModEntities;
import xyz.tytanium.ragnagoblin.core.ModSounds;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

public class RagnaGoblinEntity extends AbstractGoblinEntity
{
    public RagnaGoblinEntity(Level level)
    {
        super(ModEntities.RAGNA_GOBLIN.get(), level);
        setDespawnDelay(24000);
    }

    @Override
    public ResourceLocation getTexture()
    {
        return new ResourceLocation(Constants.MOD_ID, "textures/entity/ragna_goblin.png");
    }

    @Override
    protected void populateTradeData()
    {
        MerchantOffers offers = this.getOffers();
        EntityTrades entityTrades = TradeManager.instance().getTrades(ModEntities.RAGNA_GOBLIN.get());
        if(entityTrades != null)
        {
            Map<TradeRarity, List<VillagerTrades.ItemListing>> tradeMap = entityTrades.getTradeMap();
            for(TradeRarity rarity : TradeRarity.values())
            {
                IRaritySettings settings = Config.ENTITY.trades.getSettings(rarity);
                if(settings.includeChance() <= 0.0)
                    continue;
                if(settings.includeChance() < 1.0 && this.getRandom().nextDouble() > settings.includeChance())
                    continue;
                List<VillagerTrades.ItemListing> trades = tradeMap.get(rarity);
                int min = Math.min(settings.getMinValue(), settings.getMaxValue());
                int max = Math.max(settings.getMinValue(), settings.getMaxValue());
                int count = min + this.getRandom().nextInt(max - min + 1);
                this.addTrades(offers, trades, count, rarity.shouldShuffle());
            }
        }
    }

    @Override
    public ItemStack getFavouriteFood()
    {
        return new ItemStack(Items.POISONOUS_POTATO);
    }
    
    @Override
    public void aiStep()
    {
        if (this.isClientSide() && this.tickCount % 2 == 0)
        {
            this.level().addParticle(ParticleTypes.DRAGON_BREATH, this.getX() - 0.5 + this.getRandom().nextDouble(), this.getY() + 0.5 - 0.5 + this.getRandom().nextDouble(), this.getZ() - 0.5 + this.getRandom().nextDouble(), 0, 0, 0);
        }
        super.aiStep();
    }

    @Override
    public boolean isSensitiveToWater() {
        return true;
    }

    @Override
    protected int getMaxRestockDelay()
    {
        return Config.ENTITY.restockDelay.get();
    }

    @Override
    public boolean canAttackBack()
    {
        return Config.ENTITY.canAttackBack.get();
    }

    @Nullable
    @Override
    protected SoundEvent getAmbientSound()
    {
        return ModSounds.ENTITY_RAGNA_GOBLIN_IDLE_GRUNT.get();
    }

    @Nullable
    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn)
    {
        return ModSounds.ENTITY_RAGNA_GOBLIN_ANNOYED_GRUNT.get();
    }

    @Override
    public int getAmbientSoundInterval()
    {
        return Config.ENTITY.gruntNoiseInterval.get();
    }

    @Override
    public boolean hurt(DamageSource source, float amount)
    {
        if (this.hasCustomName())
        {
            String name = ChatFormatting.stripFormatting(this.getName().getString());
            // A Drakonik Night is invincible if riding a living entity
            if(this.isPassenger() && this.getVehicle() instanceof LivingEntity && Constants.DRAKONIK_NIGHT.equals(name))
                return false;
        }

        return super.hurt(source, amount);
    }
}
