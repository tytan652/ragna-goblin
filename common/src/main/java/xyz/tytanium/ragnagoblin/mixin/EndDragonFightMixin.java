package xyz.tytanium.ragnagoblin.mixin;

import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.boss.enderdragon.EnderDragon;
import net.minecraft.world.level.dimension.end.EndDragonFight;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import xyz.tytanium.ragnagoblin.Config;
import xyz.tytanium.ragnagoblin.Constants;
import xyz.tytanium.ragnagoblin.core.ModEntities;
import xyz.tytanium.ragnagoblin.entity.RagnaGoblinEntity;

@Mixin(EndDragonFight.class)
abstract class EndDragonFightMixin {
    @Mutable
    @Final
    @Shadow
    private ServerLevel level;
    @Shadow
    private boolean previouslyKilled;

    EndDragonFightMixin(ServerLevel level, boolean previouslyKilled) {
        this.level = level;
        this.previouslyKilled = previouslyKilled;
    }

    @Inject(method = "createNewDragon", at = @At(value = "INVOKE", target = "Lnet/minecraft/server/level/ServerLevel;addFreshEntity(Lnet/minecraft/world/entity/Entity;)Z"), locals = LocalCapture.CAPTURE_FAILHARD)
    private void drakonikNightSpawn(CallbackInfoReturnable<EnderDragon> cir, EnderDragon dragon)
    {
        // Only try to spawn on vanilla re-summoning
        if (!this.previouslyKilled)
            return;

        double randomChance = this.level.getRandom().nextDouble();
        if ((Config.ENTITY.getSpawnOnDragonChance() / 100.0) < randomChance)
            return;

        RagnaGoblinEntity goblin = ModEntities.RAGNA_GOBLIN.get().create(this.level);
        if (goblin == null)
        {
            Constants.LOG.error("Failed to generate the %s".formatted(Constants.DRAKONIK_NIGHT));
            return;
        }

        Constants.LOG.info("A Ragna Goblin decided to disrupt the re-summonning and become a %s".formatted(Constants.DRAKONIK_NIGHT));

        goblin.setCustomName(Component.Serializer.fromJson("\"%s\"".formatted(Constants.DRAKONIK_NIGHT)));
        goblin.moveTo(dragon.getX(), dragon.getY(), dragon.getZ(), dragon.getYRot(), 0.0F);
        goblin.startRiding(dragon);
        this.level.addFreshEntity(goblin);
        dragon.setCustomName(Component.Serializer.fromJson("\"%s\"".formatted(Constants.ANDER_DRAGOON)));
        dragon.setCustomNameVisible(false);
    }
}
