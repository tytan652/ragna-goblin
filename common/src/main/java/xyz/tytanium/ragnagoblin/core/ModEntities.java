package xyz.tytanium.ragnagoblin.core;

import com.mrcrayfish.framework.api.registry.RegistryContainer;
import com.mrcrayfish.framework.api.registry.RegistryEntry;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.Level;
import xyz.tytanium.ragnagoblin.Constants;
import xyz.tytanium.ragnagoblin.entity.RagnaGoblinEntity;

import java.util.function.Function;

@RegistryContainer
public class ModEntities
{
    public static final RegistryEntry<EntityType<RagnaGoblinEntity>> RAGNA_GOBLIN = build("ragna_goblin", RagnaGoblinEntity::new, 0.5F, 1.0F);

    /**
     * Author: MrCrayfish
     *
     * SPDX-License-Identifier: MIT
     */
    private static <T extends Entity> RegistryEntry<EntityType<T>> build(String name, Function<Level, T> function, float width, float height)
    {
        return RegistryEntry.entityType(new ResourceLocation(Constants.MOD_ID, name), () -> EntityType.Builder.<T>of((entityType, world) -> function.apply(world), MobCategory.CREATURE).sized(width, height).build(name));
    }
}
