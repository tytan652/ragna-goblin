package xyz.tytanium.ragnagoblin.core;

import com.mrcrayfish.framework.api.registry.RegistryContainer;
import com.mrcrayfish.framework.api.registry.RegistryEntry;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import xyz.tytanium.ragnagoblin.Constants;

@RegistryContainer
public class ModSounds
{
    public static final RegistryEntry<SoundEvent> ENTITY_RAGNA_GOBLIN_ANNOYED_GRUNT = build("entity.ragna_goblin.annoyed_grunt");
    public static final RegistryEntry<SoundEvent> ENTITY_RAGNA_GOBLIN_IDLE_GRUNT = build("entity.ragna_goblin.idle_grunt");

    /**
     * Author: MrCrayfish
     *
     * SPDX-License-Identifier: MIT
     */
    private static RegistryEntry<SoundEvent> build(String name)
    {
        return RegistryEntry.soundEvent(new ResourceLocation(Constants.MOD_ID, name), id -> () -> SoundEvent.createVariableRangeEvent(id));
    }
}
