package xyz.tytanium.ragnagoblin.core;

import com.mrcrayfish.framework.api.registry.RegistryContainer;
import com.mrcrayfish.framework.api.registry.RegistryEntry;
import com.mrcrayfish.goblintraders.platform.Services;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.SpawnEggItem;
import xyz.tytanium.ragnagoblin.Constants;

@RegistryContainer
public class ModItems
{
    public static final RegistryEntry<SpawnEggItem> RAGNA_GOBLIN_SPAWN_EGG = RegistryEntry.item(new ResourceLocation(Constants.MOD_ID, "ragna_goblin_spawn_egg"), () -> Services.PLATFORM.createSpawnEgg(ModEntities.RAGNA_GOBLIN::get, 0xEBF8B6, 0xB0AB75, new Item.Properties()));
}
