package xyz.tytanium.ragnagoblin;

import com.mrcrayfish.framework.api.event.TickEvents;
import com.mrcrayfish.goblintraders.core.ModStats;
import com.mrcrayfish.goblintraders.mixin.SpawnEggItemMixin;
import com.mrcrayfish.goblintraders.spawner.GoblinTraderSpawner;
import com.mrcrayfish.goblintraders.trades.TradeManager;
import com.mrcrayfish.goblintraders.trades.type.BasicTrade;
import net.minecraft.world.level.Level;
import xyz.tytanium.ragnagoblin.core.ModEntities;
import xyz.tytanium.ragnagoblin.core.ModItems;

public class Bootstrap
{
    public static void init()
    {
        GoblinTraderSpawner.register(ModEntities.RAGNA_GOBLIN.get(), Level.END, () -> Config.ENTITY);

        TickEvents.START_SERVER.register(server -> {
            GoblinTraderSpawner.get(server, ModEntities.RAGNA_GOBLIN.get()).ifPresent(GoblinTraderSpawner::serverTick);
        });

        TradeManager manager = TradeManager.instance();
        manager.registerTrader(ModEntities.RAGNA_GOBLIN.get());
        manager.registerTypeSerializer(BasicTrade.SERIALIZER);

        SpawnEggItemMixin.goblinTradersGetEggMap().putIfAbsent(ModEntities.RAGNA_GOBLIN.get(), ModItems.RAGNA_GOBLIN_SPAWN_EGG.get());
        SpawnEggItemMixin.goblinTradersGetEggMap().remove(null); // Remove null key
    }
}
