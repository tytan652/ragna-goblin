package xyz.tytanium.ragnagoblin.client.renderer.entity;

import com.mrcrayfish.goblintraders.Constants;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.resources.ResourceLocation;

public class RagnaGoblinModelLayer {
    public static final ModelLayerLocation RAGNA_GOBLIN = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "ragna_goblin"), "main");
}
