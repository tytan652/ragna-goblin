package xyz.tytanium.ragnagoblin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Constants
{
    public static final String MOD_ID = "ragnagoblin";

    public static final String MOD_NAME = "Ragna Goblin";

    public static final Logger LOG = LoggerFactory.getLogger(MOD_NAME);

    public static final String DRAKONIK_NIGHT = "Drakonik Night";
    public static final String ANDER_DRAGOON = "Ander Dragoon";
}
