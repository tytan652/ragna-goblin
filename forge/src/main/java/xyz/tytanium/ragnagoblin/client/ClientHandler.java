package xyz.tytanium.ragnagoblin.client;

import com.mrcrayfish.goblintraders.client.renderer.entity.GoblinTraderRenderer;
import com.mrcrayfish.goblintraders.client.renderer.entity.model.GoblinTraderModel;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import xyz.tytanium.ragnagoblin.Constants;
import xyz.tytanium.ragnagoblin.client.renderer.entity.RagnaGoblinModelLayer;
import xyz.tytanium.ragnagoblin.core.ModEntities;
import xyz.tytanium.ragnagoblin.core.ModItems;

@Mod.EventBusSubscriber(modid = Constants.MOD_ID, value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ClientHandler
{
    @SubscribeEvent
    public static void registerEntityRenderers(EntityRenderersEvent.RegisterRenderers event)
    {
        event.registerEntityRenderer(ModEntities.RAGNA_GOBLIN.get(), GoblinTraderRenderer::new);
    }

    @SubscribeEvent
    public static void registerEntityRenderers(EntityRenderersEvent.RegisterLayerDefinitions event)
    {
        event.registerLayerDefinition(RagnaGoblinModelLayer.RAGNA_GOBLIN, GoblinTraderModel::createBodyLayer);
    }

    @SubscribeEvent
    public static void onRegisterCreativeTab(BuildCreativeModeTabContentsEvent event)
    {
        if (event.getTabKey().equals(CreativeModeTabs.SPAWN_EGGS))
        {
            event.accept(ModItems.RAGNA_GOBLIN_SPAWN_EGG.get());
        }
    }
}
