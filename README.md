# Ragna Goblin

This mod is an add-on for Goblin Traders.
This is a simple mod that add a goblin you can trade with in The End dimension.

But this little guy really loves a certain mod.

Based on a "running gag" about this certain mod from the French Livestreamer and modpack maker MLDEG.

The Ragna Goblin texture is made by @Febilian, big thanks to this committer.

## Dependencies
- Goblin Traders
- Quark

## Trades
This little guy only sell "fake" Draconic Evolution item (renamed vanilla/quark items).

Those trades can be changed with datapack changes.

## I want to block his spawn
Use the NoMoWanderer mod and put "ragnagoblin:ragna_goblin" in the blacklist.