package xyz.tytanium.ragnagoblin;

import com.mrcrayfish.framework.FrameworkSetup;
import com.mrcrayfish.goblintraders.entity.AbstractGoblinEntity;
import com.mrcrayfish.goblintraders.trades.TradeManager;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.util.profiling.ProfilerFiller;
import xyz.tytanium.ragnagoblin.core.ModEntities;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class RagnaGoblin implements ModInitializer
{
    @Override
    public void onInitialize()
    {
        FrameworkSetup.run();
        Bootstrap.init();
        ResourceManagerHelper.get(PackType.SERVER_DATA).registerReloadListener(new IdentifiableResourceReloadListener()
        {
            @Override
            public ResourceLocation getFabricId()
            {
                return new ResourceLocation(Constants.MOD_ID, "trade_manager");
            }

            @Override
            public CompletableFuture<Void> reload(PreparationBarrier barrier, ResourceManager manager, ProfilerFiller profilerFiller, ProfilerFiller profilerFiller2, Executor executor, Executor executor2)
            {
                return TradeManager.instance().reload(barrier, manager, profilerFiller, profilerFiller2, executor, executor2);
            }
        });
        FabricDefaultAttributeRegistry.register(ModEntities.RAGNA_GOBLIN.get(), AbstractGoblinEntity.createAttributes());
    }
}
